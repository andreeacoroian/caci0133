package AngajatiApp;

import AngajatiApp.controller.DidacticFunction;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AgeCriteriaTest {

    Employee e1;
    Employee e2;

    @Before
    public void setUp() throws Exception {
        e1 = new Employee("Name 1", "Surname 1", "1760228000000", DidacticFunction.ASSISTENT, 400.0);
        e2 = new Employee("Name 2", "Surname 2", "2811111999999", DidacticFunction.LECTURER, 600.0);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void compareTest() {
        AgeCriteria ageCriteria = new AgeCriteria();
        int result = ageCriteria.compare(e1, e2);

        Assert.assertEquals(result, 50883);
    }
}