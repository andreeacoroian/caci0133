package AngajatiApp.repository;

import AngajatiApp.Employee;
import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.repository.EmployeeMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class EmployeeMockTest {

        private EmployeeMock employeeMock = new EmployeeMock();
        DidacticFunction df = DidacticFunction.ASSISTENT;

        @Before
        public void setUp(){
            EmployeeMock employeeMock = new EmployeeMock();
            System.out.println("Before test");
        }

        @After
        public void tearDown(){
            employeeMock = null;
            System.out.println("After test");
        }

        //blackbox
        @Test
        public void TC1() {
            Employee testEmployee = new Employee ("Coroian", "Andreea", "2891030125831",df, 10000.00);
            boolean result1 = employeeMock.addEmployee(testEmployee);
            assertTrue(result1);
        }

        @Test
        public void TC2() {
            Employee testEmployee = new Employee ("Pop", "Ion", "abc",df, 0.00);
            boolean result2 = employeeMock.addEmployee(testEmployee);
            assertFalse(result2);
        }

        @Test
        public void TC4() {
            Employee testEmployee = new Employee ("Andrei", "Antonescu", "1630506125452",df, 0.00);
            boolean result4 = employeeMock.addEmployee(testEmployee);
            assertFalse(result4);
        }

        @Test
        public void TC5() {
            Employee testEmployee = new Employee ("Mihaela", "Enescu", "286070211245",df, 0.01);
            boolean result5 = employeeMock.addEmployee(testEmployee);
            assertFalse(result5);
        }

        @Test
        public void TC6() {
            Employee testEmployee = new Employee ("Ionel", "Georgescu", "177010212125",df, 0.00);
            boolean result6 = employeeMock.addEmployee(testEmployee);
            assertFalse(result6);
        }

        @Test
        public void TC7() {
            Employee testEmployee = new Employee ("Marcu", "Ionescu", "1890705126252",df, 0.01);
            boolean result7 = employeeMock.addEmployee(testEmployee);
            assertTrue(result7);
        }


        //white box
    @Test
    public void modifyEmployeeFunction1() {
        Employee employee1 = null;
        List<Employee> employeeMockList = employeeMock.getEmployeeList();
        employeeMock.modifyEmployeeFunction(employee1, DidacticFunction.ASSISTENT);
        assertEquals(employeeMock.getEmployeeList(), employeeMockList);
    }
    @Test
    public void modifyEmployeeFunction2() {
        Employee employee1 = new Employee();
        employee1.setId(1);
        employee1.setLastName("Cordos");
        employee1.setFirstName("George");
        employee1.setCnp("1234567890876");
        employee1.setFunction(DidacticFunction.ASSISTENT);
        employee1.setSalary(2500d);
        employeeMock.addEmployee(employee1);
        employeeMock.modifyEmployeeFunction(employee1, DidacticFunction.LECTURER);
        assertTrue(employee1.getFunction() == DidacticFunction.LECTURER);
    }
    @Test
    public void modifyEmployeeFunction3() {
        Employee employee1 = new Employee();
        employee1.setId(50);
        employee1.setLastName("Coroian");
        employee1.setFirstName("Andreea");
        employee1.setCnp("2891030125831");
        employee1.setFunction(DidacticFunction.ASSISTENT);
        employee1.setSalary(3000d);
        employeeMock.modifyEmployeeFunction(employee1, DidacticFunction.LECTURER);
        assertFalse(employee1.getFunction() == DidacticFunction.LECTURER);
        assertTrue(employee1.getFunction() == DidacticFunction.ASSISTENT);
    }
}

