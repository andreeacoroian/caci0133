package AngajatiApp;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.validator.EmployeeException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeTest {
    private Employee e;

    @Before
    public void setUp() throws Exception {
        e = new Employee();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void defaultConstructorTest() {
        Employee e1 = new Employee();
        assertEquals("", e1.getCnp());
        assertEquals("", e1.getFirstName());
        assertEquals("", e1.getLastName());
        assertEquals(DidacticFunction.ASSISTENT, e1.getFunction());
        assertEquals(0, e1.getSalary(), 0.000001);
        assertEquals(0, e1.getId());
    }

    @Test
    public void initConstructorTest() {
        Employee e1 = new Employee("Name 1", "Surname 1", "1760228000000", DidacticFunction.ASSISTENT, 400.0);
        assertEquals("1760228000000", e1.getCnp());
        assertEquals("Name 1", e1.getFirstName());
        assertEquals("Surname 1", e1.getLastName());
        assertEquals(DidacticFunction.ASSISTENT, e1.getFunction());
        assertEquals(400.0, e1.getSalary(), 0.000001);
        assertEquals(0, e1.getId());
    }

    @Test
    public void setIdTest() {
        e.setId(5);
        assertEquals(5, e.getId());
    }

    @Test
    public void setFirstName() {
        e.setFirstName("Oana");
        assertEquals("Oana", e.getFirstName());
    }

    @Test (expected= EmployeeException.class, timeout = 5000l)
    public void getEmployeeFromStringTestException() throws EmployeeException {
        Employee value = Employee.getEmployeeFromString("", 0);
    }
}